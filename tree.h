#pragma once
#ifndef TWOTHREEFOURTREE_TREE_H
#define TWOTHREEFOURTREE_TREE_H

#include <iostream>
#include <cstdlib>

#include "comparable.h"
#include "treenode.h"

class key_already_exists_exception : public std::exception {
  public:
      virtual const char* what() const throw() {
          return "The key already exists!";
      }
} kaeException;

class unexpected_result_exception : public std::exception {
  public:
      virtual const char* what() const throw() {
          return "The result you got during the las operation was unexpected";
      }
} erException;

/**
 * This class represents a 2-3-4 tree itself
 * Note: Template C here is some type that derives from <code> Comparable </code>
 */
template<typename C>
class Tree {
  public:
      Tree() {
          root_ = nullptr;
      }

      /**
       * Initializes a tree with a list of keys by pushing
       * them into the tree in a given order
       * @param list is the initializer list of keys
       */
      Tree(std::initializer_list<C> list) {
          auto begin = list.begin();
          auto end = list.end();
          for (; begin != end; ++begin) {
              insert(new C(*begin));
          }
      }

      TreeNode<C>* getRoot() {
          return root_;
      }

      /**
       * Inserts the key insto the tree
       * @param key
       */
      void insert(C* key) {
          if (root_ == nullptr) {
              root_ = new TreeNode<C>(key);
          } else {
              _insert(key);
              /*if (isTracePrintable_) {
                  std::cout << std::endl << "Tree after insertions:" << std::endl;
                  print();
                  std::cout << std::endl;
              }*/
          }
      }

      void _insert(C* key) {
          TreeNode<C> foundNode;
          try {
              foundNode = search(key);
          } catch (key_already_exists_exception) {
              return;
          }
          switch (foundNode.getNumKeys()) {
              case 1:
                  if (foundNode.compareKey(MIDDLE, key) == LSS) {
                      foundNode.swapKeys(MIDDLE, RIGHT);
                      foundNode.setKey(LEFT, key);
                  } else if (foundNode.compareKey(MIDDLE, key) == GTR) {
                      foundNode.swapKeys(MIDDLE, LEFT);
                      foundNode.setKey(RIGHT, key);
                  }
                  return;
              case 2:
                  if (foundNode.compareKey(LEFT, key) == LSS) {
                      foundNode.swapKeys(LEFT, MIDDLE);
                      foundNode.setKey(LEFT, key);
                  } else if (foundNode.compareKey(LEFT, key) == GTR && foundNode.compareKey(RIGHT, key) == LSS) {
                      foundNode.setKey(MIDDLE, key);
                  } else if (foundNode.compareKey(RIGHT, key) == GTR) {
                      foundNode.swapKeys(MIDDLE, RIGHT);
                      foundNode.setKey(RIGHT, key);
                  }
                  return;
              default:
                  throw erException;
          }
      }

      TreeNode<C>& search(C* key) {
          return _search(key, nullptr, root_);
      }

      void print() {
          _print(0, root_);
      }

  private:
      TreeNode<C>* root_ = nullptr;

      TreeNode<C>& _search(C* key, TreeNode<C>* parentNode, TreeNode<C>* node) {
          if (node == root_ && node->getNumKeys() == 3) {
              _decomposeRoot();
              return _search(key, nullptr, root_);
          }
          switch (node->getNumKeys()) {
              case 1:
                  if (node->isLeaf()) {
                      return *node;
                  } else {
                      return _searchTwoChildren(key, node);
                  }
              case 2:
                  if (node->isLeaf()) {
                      return *node;
                  } else {
                      return _searchThreeChildren(key, node);
                  }
              case 3:
                  _decomposeFourChildrenNode(parentNode, node);
                  /*if (isTracePrintable_) {
                      std::cout << "\n" << "The tree after decomposition is:\n";
                      _print(0, root_);
                      std::cout << "\n";
                  }*/
                  return _search(key, nullptr, root_);
              default:
                  throw erException;
          }
      }

      TreeNode<C>& _searchTwoChildren(C* key, TreeNode<C>* node) {
          if (node->compareKey(MIDDLE, key) == LSS) {
              return _search(key, node, node->getChild(FRST));
          } else if (node->compareKey(MIDDLE, key) == GTR) {
              return _search(key, node, node->getChild(SCND));
          } else throw kaeException;
      }

      TreeNode<C>& _searchThreeChildren(C* key, TreeNode<C>* node) {
          if (node->compareKey(LEFT, key) == LSS) {
              return _search(key, node, node->getChild(FRST));
          } else if (node->compareKey(LEFT, key) == GTR && node->compareKey(RIGHT, key) == LSS) {
              return _search(key, node, node->getChild(SCND));
          } else if (node->compareKey(RIGHT, key) == GTR) {
              return _search(key, node, node->getChild(THRD));
          } else throw kaeException;
      }

      void _decomposeFourChildrenNode(TreeNode<C>* parentNode, TreeNode<C>* node) {
          C* nodeMiddleKey = node->getKey(MIDDLE);
          if (parentNode == nullptr) {
              _decomposeRoot();
              return;
          }
          switch (parentNode->getNumChildren()) {
              case TWO:
                  if (parentNode->compareKey(MIDDLE, nodeMiddleKey) == LSS) {
                      _decomposeTwoFourCase(parentNode, node, LEFT);
                  } else if (parentNode->compareKey(MIDDLE, nodeMiddleKey) == GTR) {
                      _decomposeTwoFourCase(parentNode, node, RIGHT);
                  }
                  return;
              case THREE:
                  if (parentNode->compareKey(LEFT, nodeMiddleKey) == LSS) {
                      _decomposeTwoThreeCase(parentNode, node, LEFT);
                  } else if (parentNode->compareKey(KeyPos::LEFT, nodeMiddleKey) == GTR
                             && parentNode->compareKey(KeyPos::RIGHT, nodeMiddleKey) == LSS) {
                      _decomposeTwoThreeCase(parentNode, node, MIDDLE);
                  } else if (parentNode->compareKey(KeyPos::RIGHT, nodeMiddleKey) == GTR) {
                      _decomposeTwoThreeCase(parentNode, node, RIGHT);
                  }
                  return;
              case FOUR:
              default:
                  throw std::exception();
          }
      }

      /**
       * Decomposes the root considering it is a leaf or not
       */
      void _decomposeRoot() {
          root_ = new TreeNode<C>(root_->getKey(MIDDLE),
                                  _createDecomposedChild(root_, LEFT),
                                  _createDecomposedChild(root_, RIGHT)
          );
      }

      /**
       * Decomposes 4-Node which parent is 2-Node considering
       * @param parentNode is a 2-Node parent of the node
       * @param node is a 4-Node to be composed
       * @param keyPos is key position where the middle key of node has to be inserted,
       *        takes values of KeyPos::LEFT and KeyPoS::RIGHT only!
       *
       * Note: the magic in swapChild and setChild methods when dividing keyPos by 2 is made to
       * be able of not doing additional "if"-checks.
       * Simply, when we have a left sided case we swap second and third child and set new children to
       * the first and second of parent node. Otherwise in right case we swap fourth and third child,
       * (that means we do nothing) and set second and third children of parent node.
       */
      void _decomposeTwoFourCase(TreeNode<C>* parentNode, TreeNode<C>* node, KeyPos keyPos) {
          parentNode->swapKeys(RIGHT - keyPos, MIDDLE);
          parentNode->setKey(keyPos, node->getKey(MIDDLE));
          parentNode->setNumChildren(NumOfCh::THREE);
          parentNode->swapChild(SCND + keyPos, ChPos::THRD);
          parentNode->setChild(FRST + keyPos / 2, _createDecomposedChild(node, LEFT));
          parentNode->setChild(SCND + keyPos / 2, _createDecomposedChild(node, RIGHT));
      }

      /**
       * Decomposes 4_node which parent is 3-Node considering
       * @param parentNode is a 2-Node parent of the node
       * @param node is a 4-Node to be composed
       * @param keyPos is key position where the middle key of node has to be inserted,
       *        takes values of KeyPos only!
       */
      void _decomposeTwoThreeCase(TreeNode<C>* parentNode, TreeNode<C>* node, KeyPos keyPos) {
          parentNode->swapKeys(keyPos, MIDDLE);
          parentNode->setKey(keyPos, node->getKey(MIDDLE));
          parentNode->setNumChildren(FOUR);
          if (keyPos == LEFT || keyPos == MIDDLE)
              parentNode->swapChild(THRD, FRTH);
          if (keyPos == LEFT)
              parentNode->swapChild(SCND, THRD);
          parentNode->setChild(FRST + keyPos, _createDecomposedChild(node, LEFT));
          parentNode->setChild(SCND + keyPos, _createDecomposedChild(node, RIGHT));

      }

      /**
       * Decomposes a node creating a new one by pushing
       * two left or two right child nodes to a new possible child
       * @param node is the node to be decomposed
       * @param keyPos is the position of the key, for which the children are obtained
       * @return a new possible child from the current node
       */
      TreeNode<C>* _createDecomposedChild(TreeNode<C>* node, int keyPos) {
          return node->isLeaf() ?
                 new TreeNode<C>(node->getKey(keyPos)) :
                 new TreeNode<C>(node->getKey(keyPos), node->getChild(0 + keyPos),
                                 node->getChild(1 + keyPos));
      }

      /**
       * Prints the tree in a formatted way
       * @param height is the height of the current node, increased recursively
       * @param node is the node to be printed
       */
      void _print(int height, TreeNode<C>* node) {
          std::string mod = "";
          mod += "|-";
          for (int i = 0; i < 2 * (height); mod += '-', i++);
          mod += node->toString();
          mod += "\n";
          std::cout << mod;
          for (int i = 0; i < TreeNode<C>::NODE_MAX_CHILDREN; i++) {
              if (node->getChild(i)) {
                  _print(height + 1, node->getChild(i));
              } else {
                  std::cout << "";
              }
          }
      }

};


#endif //TWOTHREEFOURTREE_TREE_H
