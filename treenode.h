#ifndef INC_2_3_4_TREES_TREENODE_H
#define INC_2_3_4_TREES_TREENODE_H

#include <iostream>

#include "comparable.h"

enum NumberOfChildren {
    NONE = 0, TWO = 2, THREE = 3, FOUR = 4
};

enum KeyPosition {
    LEFT = 0, MIDDLE = 1, RIGHT = 2
};

enum ChildPosition {
    FRST = 0, SCND = 1, THRD = 2, FRTH = 3
};

typedef NumberOfChildren NumOfCh;
typedef KeyPosition KeyPos;
typedef ChildPosition ChPos;

/**
 * This class represents a TreeNode
 */
template<typename C>
class TreeNode {
  public:
      static const int NODE_MAX_CHILDREN = 4;
      static const int NODE_MAX_KEYS = 3;

      TreeNode() {
          children_ = new TreeNode* [NODE_MAX_CHILDREN];
          keys_ = new C* [NODE_MAX_KEYS];
          numChildren_ = NumOfCh::NONE;
          _nullChildren();
          _nullKeys();
      }

      /**
       * Creates a 2-Node (contains one key and two children)
       * @param mK is the middle key of a node
       * @param leftChild contains keys that are all less than middle key
       * @param rC contains keys that are all greater than middle key
       */
      TreeNode(C* mK, TreeNode<C>* lC, TreeNode<C>* rC)
              : TreeNode(nullptr, mK, nullptr, lC, rC, nullptr, nullptr) { numChildren_ = TWO; }

      /**
       * Creates an empty 2-Node (contains one key and no children)
       * @param mK is the middle key of node
       */
      TreeNode(C* mK) : TreeNode(mK, (TreeNode<C>*) nullptr, (TreeNode*) nullptr) { numChildren_ = NONE; };

      /**
       * Creates a 3-Node (contains two keys and three children)
       * @param lK is the left key of a node
       * @param rK is the right key of a node
       * @param lC contains keys that are all less than left key
       * @param mC contains keys that are all greater than left key, but less than right key
       * @param rC contains keys that are all greater than right key
       */
      TreeNode(C* lK, C* rK, TreeNode<C>* lC, TreeNode<C>* mC, TreeNode<C>* rC)
              : TreeNode(lK, nullptr, rK, lC, mC, rC, nullptr) { numChildren_ = THREE; };

      /**
       * Creates an empty 3-Node (contains two keys and no children)
       * @param lK is the left key of a node
       * @param rK is a right key of a node
       */
      TreeNode(C* lK, C* rK) : TreeNode(lK, rK, nullptr, nullptr, nullptr) { numChildren_ = NONE; };

      TreeNode(C* lK, C* mK, C* rK, TreeNode<C>* lC, TreeNode<C>* lmC, TreeNode<C>* rmC, TreeNode<C>* rC) : TreeNode() {
          keys_[LEFT] = lK;
          keys_[MIDDLE] = mK;
          keys_[RIGHT] = rK;
          children_[FRST] = lC;
          children_[SCND] = lmC;
          children_[THRD] = rmC;
          children_[FRTH] = rC;
      }

      TreeNode(C* lK, C* mK, C* rK) : TreeNode(lK, mK, rK, nullptr, nullptr, nullptr, nullptr) { numChildren_ = NONE; }

      TreeNode<C>* getChild(int childPosition) {
          return children_[childPosition];
      }

      void setChild(int childPosition, TreeNode<C>* child) {
          children_[childPosition] = child;
      };

      C* getKey(int keyPosition) {
          return keys_[keyPosition];
      }

      void setKey(int keyPosition, C* key) {
          keys_[keyPosition] = key;
      }

      /**
       * Swaps keys beetween the positions given
       * @param lSwap is the left swapping position
       * @param rSwap is the right swapping position
       */
      void swapKeys(int lSwap, int rSwap) {
          C* tmp = keys_[lSwap];
          keys_[lSwap] = keys_[rSwap];
          keys_[rSwap] = tmp;
      }

      /**
       * Swaps the children between the positions given
       * @param lChild is the left swapping position
       * @param rChild is the right swapping position
       */
      void swapChild(int lChild, int rChild) {
          TreeNode<C>* tmp = children_[lChild];
          children_[lChild] = children_[rChild];
          children_[rChild] = tmp;
      }

      int getNumChildren() {
          return numChildren_;
      }

      void setNumChildren(NumOfCh numChildren) {
          numChildren_ = numChildren;
      }

      /**
       * @return the number of non-null keys
       */
      int getNumKeys() {
          int numKeys = 0;
          for (int i = 0; i < NODE_MAX_KEYS; i++) {
              keys_[i] ? numKeys++ : numKeys;
          }
          return numKeys;
      }

      /**
       * Compares the key of the node from keyposition with the key given
       *
       * @param keyPosition is key position
       * @param key is the key given
       * @return LSS if the key is less than keys_[keyPostion],
       *         otherwise GTR if greater or EQ if equal
       */
      int compareKey(int keyPosition, C* key) { return key->compare(keys_[keyPosition]); }

      bool isLeaf() { return numChildren_ == (int) NONE; }

      std::string toString() {
          std::string result = "";
          result += "{ ";
          result += keys_[0] ? keys_[0]->toString() : "*";
          result += ", ";
          result += keys_[1] ? keys_[1]->toString() : "*";
          result += ", ";
          result += keys_[2] ? keys_[2]->toString() : "*";
          result += " }";
          return result;
      }

  private:
      /** Array of all possible 2,3,4 children of the current node */
      TreeNode** children_ = nullptr;

      /** Array of all possible 1,2,3 keys of the current node */
      C** keys_ = nullptr;

      /** Number of children, not null children_ pointers */
      int numChildren_ = NONE;

      /** Sets all pointers to child nodes to null */
      void _nullChildren() {
          for (int i = 0; i < NODE_MAX_CHILDREN; children_[i] = nullptr, i++);
      };

      /**
       * Sets all pointers to key objects containing values to null,
       * it means when a key is going to be referenced, but was not
       * initialized at all and it is nullptr there
       */
      void _nullKeys() {
          for (int i = 0; i < NODE_MAX_KEYS; keys_[i] = nullptr, i++);
      };
};

#endif //INC_2_3_4_TREES_TREENODE_H
