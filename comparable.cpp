#include "comparable.h"

#include <sstream>

Int::Int(int i) : Comparable(i) {

}

CmpRes Int::compare(Comparable<int>* rInt) {
    if (rInt == nullptr) {
        return CmpRes::NTHNG;
    } else if (value_ == rInt->getValue()) {
        return CmpRes::EQ;
    } else {
        return value_ > rInt->getValue() ? CmpRes::GTR : CmpRes::LSS;
    }
}

std::string Int::toString() {
    std::stringstream resultStream;
    resultStream << value_;
    return resultStream.str();
}

Char::Char(char c) : Comparable(c) {

}

CmpRes Char::compare(Comparable<char>* rChar) {
    if (rChar == nullptr) {
        return CmpRes::NTHNG;
    } else if ((int) value_ == (int) rChar->getValue()) {
        return CmpRes::EQ;
    } else {
        return (int) value_ > (int) rChar->getValue() ? CmpRes::GTR : CmpRes::LSS;
    }
}

std::string Char::toString() {
    return std::string() += value_;
}
