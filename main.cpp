#include "comparable.h"
#include "tree.h"

void test_char_tree() {
    std::cout << std::endl << "Testing CHAR tree" << std::endl;
    Tree<Char> tree = {'A', 'S', 'R', 'C', 'H', 'I', 'N', 'G', 'E', 'X', 'M', 'P', 'L', 'Y', 'W', 'Z'};
    tree.print();
}

void test_int_tree() {
    std::cout << std::endl << "Testing INT tree" << std::endl;
    Tree<Int> tree;
    tree = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 2, 3, 1678, 345, 23,
            16, 5, 567, 3, 4, 5, 6, 7, 3, 35, 68, 94, 53, 26,
            657, 345, 678, 870, 269, 10000};
    tree.print();
    std::cout << std::endl;
}

void test_int_tree_sorted() {
    std::cout << std::endl << "Testing INT sorted tree" << std::endl;
    Tree<Int> tree;
    tree = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    tree.print();
    std::cout << std::endl;
}

void test_char_tree_sorted() {
    std::cout << std::endl << "Testing CHAR sorted tree" << std::endl;
    Tree<Char> tree = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'};
    tree.print();
}

int main() {
    test_char_tree();
    test_int_tree();
    test_char_tree_sorted();
    test_int_tree_sorted();
}