#ifndef INC_2_3_4_TREES_COMPARABLE_H
#define INC_2_3_4_TREES_COMPARABLE_H

#include <iostream>

enum CompareResult {
    NTHNG = 0, LSS = 1, EQ = 2, GTR = 3
};
typedef CompareResult CmpRes;

/**
 * This abstract class represents a highier level of abstraction
 * above the primitive types. It helps to specify a box
 * for a primitive and give all such boxes two(or more)
 * valuable options -- compare boxes and output them to streams
 * Note: Template Type here is a primitive (or basic) class,
 * but in classes TreeNode and Tree template types are
 * classes that derive from Comprable.
 */
template<class Type>
class Comparable {
  protected:
      /** The value of the specific type that is being boxed into a box-class */
      Type value_ = (Type) 0;
  public:

      /**
       * Empty constructor
       */
      Comparable<Type>() {};

      /**
       * Constructor that takes the value and puts it into "the box"
       * @param value for the value being boxed
       */
      Comparable<Type>(Type value) { value_ = value; }

      Type getValue() { return value_; }

      void setValue(Type value) { value_ = value; }

      /**
       * Assigment operator that helps assigning a Type value directly
       * into the boxed class instance.
       * For example, if class Int derives from Comparable<int>,
       * expression "Int num = 3" will compile
       * @param rvalue for the value of type Type that is assigned
       * @return this instance but the value_ field changed to rvalue
       */
      Comparable<Type>*& operator=(const Type rvalue) {
          value_ = rvalue;
          return this;
      }

      /**
       * A pure virtual function that compares two objects and
       * returns one of the result codes from CompareResult enum
       * @param rvalue for the object we compare with
       * @return result code whether this object is less, equals or greater than another,
       * otherwise, if another is null, return result code NONE.
       */
      virtual CmpRes compare(Comparable<Type>* rvalue) = 0;

      /**
       * A pure virtual operator that outputs the object to the given
       * output stream
       * @param os for output stream
       * @return current output stream for future chained calls
       */
      virtual std::string toString() = 0;
};

/**
 * A struct that boxes all numbers of type int
 * into a struct, an implementation of abstract
 * Comparable<Type> where Type = int
 */
class Int : public Comparable<int> {
  public:
      Int(int i);

      std::string toString() override;

      CmpRes compare(Comparable<int>* rInt) override;
};

class Char : public Comparable<char> {
  public:
      Char(char c);

      std::string toString() override;

      CmpRes compare(Comparable<char>* rChar) override;
};

#endif //INC_2_3_4_TREES_TYPE_H
